package com.company;

import com.company.menu.InitialMenu;
import com.company.menu.MainMenu;
import com.company.library.Library;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Library library = new Library();

        InitialMenu initialMenu = new InitialMenu(scanner);
        initialMenu.SetLibrary();

        MainMenu mainMenu  = new MainMenu(scanner, library);
        mainMenu.ShowMenu();
    }
}
