package com.company.library;

import com.company.models.Book;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Library {

    private List<Book> books;

    public Library(){
        this.books = new ArrayList<>();
    }

    public boolean AddBook(Book book){
        books.add(book);

        return true;
    }

    public void RemoveBook(Book book) {
        books.remove(book);
    }

    public Book SearchBook(Predicate<Book> predicate) {
        var toReturn = books.stream().filter(predicate).findFirst();
        return toReturn.orElse(null);
    }

    public List<Book> ListBooks() {
        return  this.books;
    }
}
