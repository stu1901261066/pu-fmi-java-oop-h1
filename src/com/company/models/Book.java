package com.company.models;

import java.time.Duration;
import java.time.LocalDate;
import java.util.List;

public class Book {
    private String title;
    private String genre;
    private List<String> authors;
    private String publisher;
    private int publishYear;
    private String ISBN;
    private int pageCount;
    private String language;
    private boolean isAvailable;
    private LocalDate bookedDate;
    private LocalDate returnDate;
    private Duration bookingDuration;
    private int bookingCount;


    public String getTitle() {
        return title;
    }

    public boolean setTitle(String title) {
        if(title.length() >= 100) return false;

        this.title = title;

        return true;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public boolean setAuthors(List<String> authors) {
        if(authors.stream().anyMatch(author -> author.length() >= 50)) return false;

        this.authors = authors;

        return true;
    }

    public String getPublisher() {
        return publisher;
    }

    public boolean setPublisher(String publisher) {
        if(publisher.length() >= 50) return false;

        this.publisher = publisher;

        return true;
    }

    public int getPublishYear() {
        return publishYear;
    }

    public boolean setPublishYear(int publishYear) {
        if(publishYear < 0 || publishYear > LocalDate.now().getYear()) return false;

        this.publishYear = publishYear;

        return true;
    }

    public String getISBN() {
        return ISBN;
    }

    public boolean setISBN(String ISBN) {
        if(ISBN.length() >= 10) return false;

        this.ISBN = ISBN;

        return  true;
    }

    public int getPageCount() {
        return pageCount;
    }

    public boolean setPageCount(int pageCount) {
        if(pageCount < -1 && pageCount <= 0) return false;

        this.pageCount = pageCount;

        return true;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public LocalDate getBookedDate() {
        return bookedDate;
    }

    public void setBookedDate(LocalDate bookedDate) {
        this.bookedDate = bookedDate;
    }

    public LocalDate getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(LocalDate returnDate) {
        this.returnDate = returnDate;
    }

    public Duration getBookingDuration() {
        return bookingDuration;
    }

    public void setBookingDuration(Duration bookingDuration) {
        this.bookingDuration = bookingDuration;
    }

    public int getBookingCount() {
        return bookingCount;
    }

    public boolean setBookingCount(int bookingCount) {
        if(bookingCount < 0) return false;

        this.bookingCount = bookingCount;

        return true;
    }

    @Override
    public String toString() {
        return "title=" + title +
                ", genre=" + genre +
                ", authors=" + authors +
                ", publisher=" + publisher +
                ", publishYear=" + publishYear +
                ", ISBN=" + ISBN +
                ", pageCount=" + pageCount +
                ", language=" + language +
                ", isAvailable=" + isAvailable +
                ", bookedDate=" + bookedDate +
                ", returnDate=" + returnDate +
                ", bookingDuration=" + bookingDuration +
                ", bookingCount=" + bookingCount;
    }
}
