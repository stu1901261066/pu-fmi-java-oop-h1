package com.company.models;

import java.util.List;

public class Library {
    private String name;
    private String address;
    private int employeeCount;
    private List<Book> books;

    public String getName() {
        return name;
    }

    public boolean setName(String name) {
        if(name.length() >=100 ) return false;

        this.name = name;

        return true;
    }

    public String getAddress() {
        return address;
    }

    public boolean setAddress(String address) {
        if(address.length() >= 500) return false;

        this.address = address;

        return true;
    }

    public int getEmployeeCount() {
        return employeeCount;
    }

    public boolean setEmployeeCount(int employeeCount) {
        if(employeeCount < 0 || employeeCount >= 50) return false;

        this.employeeCount = employeeCount;

        return  true;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public void AddBook(Book addBook) {
    }
}
