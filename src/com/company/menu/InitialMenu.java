package com.company.menu;

import com.company.models.Library;

import java.util.Scanner;

public class InitialMenu {

    private Library library;
    private Scanner scanner;

    public InitialMenu(Scanner scanner)
    {
        this.scanner = scanner;
        this.library = new Library();
        System.out.println("Insert library Information!");
    }

    public void SetLibrary()
    {
        InsertName();
        InsertAddress();
        InsertEmployeeCount();
    }

    public Library GetLibrary()
    {
        return this.library;
    }

    private void InsertName(){
        System.out.print("Insert Name: ");

        boolean isSet;

        do{

            String name = scanner.nextLine();
            isSet = library.setName(name);
            if(!isSet) System.out.println("You stupid!");

        } while (!isSet);
    }

    private void InsertAddress(){
        System.out.print("Insert Address: ");

        boolean isSet;

        do{

            String name = scanner.nextLine();
            isSet = library.setAddress(name);
            if(!isSet) System.out.println("You stupid!");

        } while (!isSet);
    }

    private void InsertEmployeeCount(){
        System.out.print("Insert Number of Employees: ");

        boolean isSet;

        do{

            int count = scanner.nextInt();
            isSet = library.setEmployeeCount(count);
            if(!isSet) System.out.println("You stupid!");

        } while (!isSet);
    }
}
