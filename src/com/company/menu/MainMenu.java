package com.company.menu;

import com.company.models.Book;
import com.company.library.Library;

import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class MainMenu {

    private Scanner scanner;
    private Library library;

    public MainMenu(Scanner scanner, Library library){

        this.scanner = scanner;
        this.library = library;
    }

    public void ShowMenu(){
        boolean isActive = true;

        while (isActive){
            PrintMenuOptions();
            int userInput = ReadInput(6);
            switch (userInput) {
                case 1 -> library.AddBook(AddBook());
                case 2 -> library.RemoveBook(RemoveBook());
                case 3 -> SearchBook();
                case 4 -> UpdateBook();
                case 5 -> ListBooks(library.ListBooks());
                case 6 -> isActive = false;
            }
        }
    }

    private void PrintMenuOptions(){
        System.out.println("1. Add Book");
        System.out.println("2. Remove Book");
        System.out.println("3. Search Book");
        System.out.println("4. Update Book");
        System.out.println("5. List Books");
        System.out.println("6. Exit");
    }

    private int ReadInput(int maxOption){
        System.out.print("Select option: ");

        boolean isInvalid = false;
        int selectedOption = 0;

        do{
            String line = scanner.nextLine();

            try{
                selectedOption = Integer.parseInt(line);
                isInvalid = false;
            } catch (Exception e)
            {
                isInvalid = true;
            }

            if(selectedOption > maxOption) isInvalid = true;
            if(selectedOption < 1 ) isInvalid = true;

        }while (isInvalid);

        return selectedOption;
    }

    private Book AddBook(){
        Book newBook = new Book();
        InsertTitle(newBook);
        InsertAuthor(newBook);
        InsertGenre(newBook);
        InsertLanguage(newBook);
        InsertPublisher(newBook);
        InsertPublishDate(newBook);
        InsertISBN(newBook);
        InsertPageCount(newBook);
        InsertAvailability(newBook);
        InsertBookingCount(newBook);

        return newBook;
    }

    private Book RemoveBook(){
        Book toFind = new Book();

        System.out.println("1.Search by Title");
        System.out.println("2.Search by ISBN");

        int selectedOption = ReadInput(2);

        Book toDelete;
        if(selectedOption == 1)
        {
            InsertTitle(toFind);
            toDelete = library.SearchBook(book -> book.getTitle().equals(toFind.getTitle()));
        }
        else
        {
            InsertISBN(toFind);
            toDelete = library.SearchBook(book -> book.getISBN().equals(toFind.getISBN()));
        }

        return toDelete;
    }

    private void SearchBook(){
        Book toFind = new Book();

        System.out.println("1.Search by Title");
        System.out.println("2.Search by Author");
        System.out.println("3.Search by Year");
        System.out.println("4.Search by ISBN");

        int selectedOption = ReadInput(4);

        switch (selectedOption) {
            case 1 -> {
                InsertTitle(toFind);
                System.out.println(library.SearchBook(book -> book.getTitle().equals(toFind.getTitle())));
            }
            case 2 -> {
                InsertAuthor(toFind);

                List<Book> allBooks = library.ListBooks();
                List<String> searchedAuthors = toFind.getAuthors();
                for (Book bk: allBooks ) {
                    List<String> authors = bk.getAuthors();
                    for(String author: authors){
                        boolean isFound = searchedAuthors.stream().anyMatch(a -> a.equals(author));
                        if(isFound) {
                            System.out.println(bk);
                            return;
                        }
                    }
                }
            }
            case 3 -> {
                InsertPublishDate(toFind);
                System.out.println(library.SearchBook(book -> book.getPublishYear() == toFind.getPublishYear()));
            }
            case 4 -> {
                InsertISBN(toFind);
                System.out.println(library.SearchBook(book -> book.getISBN().equals(toFind.getISBN())));
            }
        }
    }

    private void UpdateBook(){
        Book newBook = new Book();
        InsertISBN(newBook);

        Book searchedBook = library.SearchBook(book -> book.getISBN().equals(newBook.getISBN()));


        if(searchedBook == null) return;

        System.out.println("1.Update Availability");
        System.out.println("2.Update Booking Date");
        System.out.println("3.Update Return Date");
        System.out.println("4.Update Booking Duration");
        System.out.println("5.Update Booking Times");

        int selectedOption = ReadInput(5);

        switch (selectedOption){
            case 1 -> InsertAvailability(searchedBook);
            case 2 -> InsertBookingDate(searchedBook);
            case 3 -> InsertReturnDate(searchedBook);
            case 4 -> InsertBookingDuration(searchedBook);
            case 5 -> InsertBookingTimes(searchedBook);
        }
    }

    private void InsertBookingTimes(Book book) {
        System.out.print("Insert number of Bookings: ");

        boolean isSet;
        do{

            String line = scanner.nextLine();
            int count;
            try{

                count = Integer.parseInt(line);
                isSet = book.setBookingCount(count);

            } catch (Exception e){ isSet = false; }

            if(!isSet) System.out.println("Wrong Input!");

        } while (!isSet);
    }

    private void InsertBookingDuration(Book book) {
        System.out.print("Insert Booking Duration in days: ");
        boolean isSet;
        do {

            String line = scanner.nextLine();
            int months;
            try {

                months = Integer.parseInt(line);
                isSet = true;

                book.setBookingDuration(Duration.ofDays(months));

            } catch (Exception e){ isSet = false; }

            if(!isSet) System.out.println("Wrong Input!");

        } while (!isSet);
    }

    private void InsertReturnDate(Book book) {
        System.out.print("Insert Return Date: ");

        boolean isSet;
        do{

            String line = scanner.nextLine();
            LocalDate date;
            try{

                date = LocalDate.parse(line);
                isSet = true;

                book.setReturnDate(date);

            }catch (Exception e){
                isSet = false;
            }
            if(!isSet) System.out.println("Wrong Input!");

        } while (!isSet);
    }

    private void InsertBookingDate(Book book) {
        System.out.print("Insert Booking Date: ");

        boolean isSet;
        do{

            String line = scanner.nextLine();
            LocalDate date;
            try{

                date = LocalDate.parse(line);
                isSet = true;

                book.setBookedDate(date);

            }catch (Exception e){
                isSet = false;
            }
            if(!isSet) System.out.println("Wrong Input!");

        } while (!isSet);
    }

    private void ListBooks(List<Book> books){
        books.forEach(System.out::println);
    }

    private void InsertTitle(Book book){
        System.out.print("Insert Name: ");

        boolean isSet;
        do{

            String title = scanner.nextLine();
            isSet = book.setTitle(title);
            if(!isSet) System.out.println("Wrong Input!");

        } while (!isSet);
    }

    private void InsertGenre(Book book){
        System.out.print("Insert Genre: ");

        String line = scanner.nextLine();
        book.setGenre(line);
    }

    private void InsertLanguage(Book book){
        System.out.print("Insert Language: ");

        String line = scanner.nextLine();
        book.setLanguage(line);
    }

    private void InsertAuthor(Book book){
        System.out.print("Insert Authors separated with a comma and leave a space after the comma: ");

        boolean isSet;
        do{

            String author = scanner.nextLine();

            var authors = Arrays.stream(author.split(", ")).toList();
            //for (int i = 0;i < authors.size(); i++) authors.set(i,authors.get(i).trim());

            isSet = book.setAuthors(authors);
            if(!isSet) System.out.println("Wrong Input!");

        } while (!isSet);
    }

    private void InsertPublisher(Book book){
        System.out.print("Insert Publisher: ");

        boolean isSet;
        do{

            String publisher = scanner.nextLine();
            isSet = book.setPublisher(publisher);
            if(!isSet) System.out.println("Wrong Input!");

        } while (!isSet);
    }

    private void InsertPublishDate(Book book){
        System.out.print("Insert Year of Publishing: ");

        boolean isSet;
        do{

            String line = scanner.nextLine();
            int year;
            try{

                year = Integer.parseInt(line);
                isSet = book.setPublishYear(year);

            }catch (Exception e){
                isSet = false;
            }
            if(!isSet) System.out.println("Wrong Input!");

        } while (!isSet);
    }

    private void InsertISBN(Book book){
        System.out.print("Insert ISBN: ");

        boolean isSet;
        do{

            String isbn = scanner.nextLine();
            isSet = book.setISBN(isbn);
            if(!isSet) System.out.println("Wrong Input!");

        } while (!isSet);
    }

    private void InsertPageCount(Book book){
        System.out.print("Insert the Number of Pages: ");

        boolean isSet;
        do{

            String title = scanner.nextLine();
            int pageCount;
            try{

                pageCount = Integer.parseInt(title);
                isSet = book.setPageCount(pageCount);

            }catch (Exception e){ isSet = false; }

            if(!isSet) System.out.println("Wrong Input!");

        } while (!isSet);
    }

    private void InsertAvailability(Book book){
        System.out.print("Insert Availability: ");

        boolean isSet = true;
        boolean available = false;
        do{

            String line = scanner.nextLine();
            line = line.toLowerCase();
            if(line.equals("yes")) available = true;
            if(line.equals("no")) available = false;

            book.setAvailable(available);

            if(!(line.equals("yes") || line.equals("no"))) isSet = false;

            if(!isSet) System.out.println("Wrong Input!");

        } while (!isSet);
    }

    private void InsertBookingCount(Book book){
        System.out.print("Insert Times Booked: ");

        boolean isSet;
        do{
            String line = scanner.nextLine();
            int bookingCount;
            try{

                bookingCount = Integer.parseInt(line);
                isSet = book.setBookingCount(bookingCount);

            }catch (Exception e){ isSet = false; }

            if(!isSet) System.out.println("Wrong Input!");

        } while (!isSet);
    }
}
